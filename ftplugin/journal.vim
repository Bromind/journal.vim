" Plugin Vim pour journaliser ses idées.
" Last Change:	2018 July 09
" Maintainer:	Martin Vassor <martin@vassor.org>
" License:	WTFPL

if exists("b:did_ftplugin")
	finish
endif
let b:did_ftplugin = 1
if exists("g:loaded_journal")
	finish
endif
let g:loaded_journal = 1

if !hasmapto('<Plug>JournalAjout')
	map <unique> <Leader>b <Plug>JournalAjout
endif
if !hasmapto('<Plug>JournalSuivant')
	map <unique> <Leader>n <Plug>JournalSuivant
endif

noremap <unique> <script> <Plug>JournalAjout <SID>Ajout
noremap <SID>Ajout :call <SID>Ajout()<CR>

noremap <unique> <script> <Plug>JournalSuivant <SID>Suivant
noremap <SID>Suivant :call <SID>Suivant()<CR>

set foldmethod=marker

function s:Ajout()
	:normal gg
	call append(line("."), "}}}")
	call append(line("."), "<++>")
	call append(line("."), "Contenu :")
	call append(line("."), "Date : " . strftime("%c"))
	call append(line("."), "{{{Titre : <++>")
	call append(line("."), "")

	call <SID>Suivant()
endfunction

function s:Suivant()
	:normal zM
	let [l, c] = searchpos("<++>")
	call cursor(l, c)
	:normal zv
endfunction


